/*
    Separation of Concerns
    Models Folder
    - Contain the Object Schemas and defines the object structure and content

    Controllers Folder
    - Contain the functions and business logic of our Express JS application
    - Meaning all the operations it can do will be placed in this file

    Routes Folder
    - Contains all the endpoints for our application
    - We separate the routes such that "index.js" only contains information on the server
            
        require -> to include a specific module/package.
        module.exports -> to treat a value as a "module" that can be used by other files

        Flow of exports and require: 
        export models > require in controllers 
        export controllers > require in routes
        export routes > require in app.js
*/



// require the installed modules
const express = require("express");
const mongoose = require("mongoose");

// port
const port = 4000;

// server
const app = express();

// middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));


// connect to database (MongoDB)
mongoose.connect("mongodb+srv://admin:admin@zuitt-bootcamp.7jqbank.mongodb.net/batch203_to-do?retryWrites=true&w=majority",
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

let db = mongoose.connection;

// Captures and give details of the error when connecting to mongoose
db.on("error", console.error.bind(console, "connection error"));

// Successful connection and can made queries
db.once("open", () => console.log("We're connected to the cloud database."));

// Routes Grouping - organize the access for each resources.
const taskRoutes = require("./routes/taskRoutes");
app.use("/tasks", taskRoutes); //localhost:4000/tasks/



// port listener
app.listen(port, () => console.log(`Server is running at ${port}`));


const express = require("express");

// express.Router() method allows access to HTTP methods.
const router = express.Router();

const taskControllers = require("../controllers/taskControllers");

console.log(taskControllers);

// Create task routes
// CREATE TASK
router.post("/", taskControllers.createTask);

// VIEW TASKS
// router.get("/", taskControllers.viewTasks);

// GET ALL TASKS
router.get("/allTasks", taskControllers.getAllTasksController);

// GET A SINGLE TASK
router.get("/getSingleTask/:taskId", taskControllers.getSingleTaskController);

// UPDATING TASK STATUS
router.patch("/updateTask/:taskId", taskControllers.updateTaskStatusController);

// DELETE A TASK
router.delete("/deleteTask/:taskId", taskControllers.deleteTaskController);








// this will be used in our server
module.exports = router;
// import the Task model in our controllers so that our controllers function have access to our Task Model (method, schema)
const Task = require("../models/Task");

/* 
    Syntax:
        app.httpMethods("/endpoint", (anonymousFunction));
*/

// Create task: create a task without duplicate names.
console.log("This is controllers");
module.exports.createTask = (req, res) => {
    // checking captured data from the request body
    console.log(req.body);

    Task.findOne({name: req.body.name}).then(result => {
        console.log(result);

        if (result != null && result.name == req.body.name){
            return res.send("Duplicate task found")
        }
        else {
            let newTask = new Task({
                name: req.body.name,
            })

            newTask.save()
            .then(result => res.status(201).send(result))
            .catch(error => res.send(error));
            
        }
    })
    .catch(error => res.send(error));
};

// GET ALL TASKS ===== MINI ACTIVITY
/*
module.exports.viewTasks = (req, res) => {
    console.log(req.body);
    Task.find({}).then(result => {
        if (result) {
            return res.send({
                data: result
            })
        }
    })
    .catch(error => res.send(error));
};
*/


// RETRIEVE ALL TASKS

module.exports.getAllTasksController = (req, res) => {
    Task.find({})
    .then(tasks => res.send(tasks))
    .catch(error => res.send(error))
};



// RETRIEVE A SINGLE TASK
module.exports.getSingleTaskController = (req, res) => {
    console.log(req.params);

    Task.findById(req.params.taskId)
    .then(task => res.send(task))
    .catch(error => res.send(error))
};



// UPDATING TASK STATUS
module.exports.updateTaskStatusController = (req, res) => {
    console.log(req.params.taskId);
    console.log(req.body);

    // variable containing the object parameter
    let updates = {
        status: req.body.status
    };

    // Syntax: findByIdAndUpdate(_id, {objectUpdate}, options)
        // {new: true} - returns the updated version of the document.
    Task.findByIdAndUpdate(req.params.taskId, updates, {new: true})
    .then(updatedTask => res.send(updatedTask))
    .catch(error => res.send(error))
};


// DELETE A TASK
module.exports.deleteTaskController = (req, res) => {
    console.log(req.params.taskId); // for checking result

    Task.findByIdAndRemove(req.params.taskId)
    .then(removedTask => res.send(removedTask))
    .catch(error => res.send(error))
};